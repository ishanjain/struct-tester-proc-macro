use {
    proc_macro::TokenStream,
    proc_macro2::{Punct, Spacing, TokenStream as TokenStream2},
    quote::{quote, ToTokens, TokenStreamExt},
    std::convert::From,
    syn::{
        bracketed,
        parse::{Parse, ParseStream},
        parse_macro_input,
        punctuated::Punctuated,
        token::Bracket,
        Ident, LitBool, LitInt, LitStr, Result as SynResult, Token,
    },
};

#[derive(Debug)]
struct FnInputOutput {
    function_names_bracket: Bracket,
    function_names: Punctuated<FunctionName, Token![,]>,
    first_comma: Token![,],
    inputs_bracket: Bracket,
    inputs: Punctuated<Inputs, Token![,]>,
    second_comma: Token![,],
    outputs_bracket: Bracket,
    outputs: Punctuated<OptionalArg, Token![,]>,
}

impl Parse for FnInputOutput {
    fn parse(input: ParseStream) -> SynResult<Self> {
        let function_names_content;
        let inputs_content;
        let outputs_content;

        Ok(Self {
            function_names_bracket: bracketed!(function_names_content in input),
            function_names: Punctuated::parse_terminated(&function_names_content)?,
            first_comma: input.parse()?,
            inputs_bracket: bracketed!(inputs_content in input),
            inputs: Punctuated::parse_terminated(&inputs_content)?,
            second_comma: input.parse()?,
            outputs_bracket: bracketed!(outputs_content in input),
            outputs: Punctuated::parse_terminated(&outputs_content)?,
        })
    }
}

#[derive(Debug)]
struct FunctionName(Ident);

impl Parse for FunctionName {
    fn parse(input: ParseStream) -> SynResult<Self> {
        if input.peek(LitStr) {
            Ok(Self(input.parse::<LitStr>()?.parse::<Ident>()?))
        } else {
            Err(input.error("expected a string"))
        }
    }
}

impl ToTokens for FunctionName {
    fn to_tokens(&self, token_stream: &mut TokenStream2) {
        token_stream.append(self.0.clone());
    }
}

#[derive(Debug)]
struct Inputs {
    bracket: Bracket,
    args: Punctuated<Arg, Token![,]>,
}

impl Parse for Inputs {
    fn parse(input: ParseStream) -> SynResult<Self> {
        let content;
        Ok(Self {
            bracket: bracketed!(content in input),
            args: Punctuated::parse_terminated(&content)?,
        })
    }
}

impl ToTokens for Inputs {
    fn to_tokens(&self, token_stream: &mut TokenStream2) {
        for arg in self.args.iter() {
            arg.to_tokens(token_stream);
            token_stream.append(Punct::new(',', Spacing::Joint));
        }
    }
}

#[derive(Debug)]
enum Arg {
    String(LitStr),
    Integer(LitInt),
}

impl Parse for Arg {
    fn parse(input: ParseStream) -> SynResult<Self> {
        if input.peek(LitInt) {
            Ok(Self::Integer(input.parse()?))
        } else if input.peek(LitStr) {
            Ok(Self::String(input.parse()?))
        } else {
            Err(input.error("expected a string or a integer"))
        }
    }
}

impl ToTokens for Arg {
    fn to_tokens(&self, token_stream: &mut TokenStream2) {
        match self {
            Arg::String(v) => token_stream.extend(quote! ( #v.into())),
            Arg::Integer(v) => v.to_tokens(token_stream),
        }
    }
}

#[derive(Debug)]
enum OptionalArg {
    String(LitStr),
    Integer(LitInt),
    Bool(LitBool),
    None(keywords::None),
}

impl Parse for OptionalArg {
    fn parse(input: ParseStream) -> SynResult<Self> {
        if input.peek(LitStr) {
            Ok(Self::String(input.parse()?))
        } else if input.peek(LitInt) {
            Ok(Self::Integer(input.parse()?))
        } else if input.peek(LitBool) {
            Ok(Self::Bool(input.parse()?))
        } else if input.peek(keywords::None) {
            Ok(Self::None(input.parse()?))
        } else {
            Err(input.error("expected a string or integer or None"))
        }
    }
}

impl ToTokens for OptionalArg {
    fn to_tokens(&self, token_stream: &mut TokenStream2) {
        match self {
            OptionalArg::String(v) => v.to_tokens(token_stream),
            OptionalArg::Integer(v) => v.to_tokens(token_stream),
            OptionalArg::Bool(v) => v.to_tokens(token_stream),
            OptionalArg::None(v) => v.to_tokens(token_stream),
        }
    }
}

mod keywords {
    use syn::custom_keyword;
    custom_keyword!(None);
}

#[proc_macro]
pub fn struct_tester(input: TokenStream) -> TokenStream {
    let parsed_input = parse_macro_input!(input as FnInputOutput);
    let mut fnames = parsed_input.function_names.into_iter();
    let mut inputs = parsed_input.inputs.into_iter();
    let mut outputs = parsed_input.outputs.into_iter();

    let mut out = {
        let fname = fnames.next().unwrap();
        let args = inputs.next().unwrap();
        let _output = outputs.next().unwrap();

        quote! {
            let mut instance = #fname::new(#args);
        }
    };

    for (fname, (input, output)) in fnames.zip(inputs.zip(outputs)) {
        if let OptionalArg::None(_) = output {
            out.extend(quote! {
                instance.#fname(#input);
            });
        } else {
            out.extend(quote! {
                let got_output = instance.#fname(#input);
                assert_eq!(got_output, #output);
            });
        }
    }
    TokenStream::from(out)
}
