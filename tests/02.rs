#![feature(proc_macro_hygiene)]
struct CombinationIterator {
    permutations: Vec<String>,
}

impl CombinationIterator {
    fn new(characters: String, length: i32) -> Self {
        let mut permutations = compute_permutations(characters.chars().collect(), length as usize);

        permutations.sort();
        let permutations: Vec<String> = permutations.into_iter().rev().collect();

        Self { permutations }
    }

    fn next(&mut self) -> String {
        self.permutations.pop().unwrap()
    }

    fn has_next(&self) -> bool {
        !self.permutations.is_empty()
    }
}

fn compute_permutations(input: Vec<char>, length: usize) -> Vec<String> {
    let mut permutations = vec![];
    let mut permute = String::new();

    permute_helper(&input, &mut permutations, &mut permute, length, 0);

    permutations
}

fn permute_helper(
    input: &[char],
    permutations: &mut Vec<String>,
    permute: &mut String,
    length: usize,
    start: usize,
) {
    if permute.len() == length {
        let mut spermute: Vec<char> = permute.chars().collect();
        spermute.sort();

        for (i, j) in permute.chars().zip(spermute.into_iter()) {
            if i != j {
                return;
            }
        }
        permutations.push(permute.clone());
    } else {
        for i in start..input.len() {
            permute.push(input[i]);

            permute_helper(&input, permutations, permute, length, i + 1);

            permute.pop();
        }
    }
}

use struct_tester::struct_tester;

#[test]
fn test() {
    struct_tester!(
        [
            "CombinationIterator",
            "next",
            "has_next",
            "next",
            "has_next",
            "next",
            "has_next"
        ],
        [["abc", 2], [], [], [], [], [], []],
        [None, "ab", true, "ac", true, "bc", false]
    );
}
