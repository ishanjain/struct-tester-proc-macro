#![feature(proc_macro_hygiene)]

use struct_tester::struct_tester;
struct MinStack {
    stack: Vec<i32>,
    min: i32,
}

impl MinStack {
    pub fn new() -> Self {
        MinStack {
            stack: vec![],
            min: std::i32::MAX,
        }
    }

    pub fn push(&mut self, x: i32) {
        self.stack.push(x);
        if x < self.min {
            self.min = x;
        }
    }

    pub fn pop(&mut self) {
        self.stack.pop();
        self.set_min();
    }

    fn set_min(&mut self) {
        let mut m = std::i32::MAX;
        for i in &self.stack {
            if *i < m {
                m = *i;
            }
        }
        self.min = m;
    }

    pub fn top(&self) -> i32 {
        self.stack[self.stack.len() - 1]
    }

    pub fn get_min(&self) -> i32 {
        self.min
    }
}
#[test]
fn main() {
    struct_tester!(
        ["MinStack", "push", "push", "push", "get_min", "pop", "top", "get_min"],
        [[], [-2], [0], [-3], [], [], [], []],
        [None, None, None, None, -3, None, 0, -2]
    );
}
