# Struct Tester

This macro takes a list of function calls to make, The input those calls will take and their outputs and converts it into rust code.

Example code:

```rust
struct_tester!(
    ["MinStack", "push", "push", "push", "get_min", "pop", "top", "get_min"],
    [[], [-2], [0], [-3], [], [], [], []],
    [None, None, None, None, -3, None, 0, -2]
);
```

This example code will be translated to,

```rust
let mut instance = MinStack::new();
instance.push(-2);
instance.push(0);
instance.push(-3);
let got_output = instance.get_min();
assert_eq!(-3, got_output);
instance.pop();
let got_output = instance.top();
assert_eq!(0, got_output);
let got_output = instance.get_min();
assert_eq!(-2, got_output);
```


Please let me know if you have any feedback! :)) [twitter](https://twitter.com/ishanjain28)/ishanjain.me
